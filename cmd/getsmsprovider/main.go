package main

import (
	"context"
	"log"
	"time"

	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/chromedp"
	"gitlab.com/remotejob/getsmsprovider/internal/config"
	"gitlab.com/remotejob/getsmsprovider/pkg/dbhnadler"
	"gitlab.com/remotejob/getsmsprovider/pkg/elabnodes"
	"gitlab.com/remotejob/getsmsprovider/pkg/splitphonenum"

	_ "github.com/mattn/go-sqlite3"
)

var (
	err        error
	phnumsplit []string
	nodes      []*cdp.Node
	conf       *config.Config
	phnums     []string
)

func init() {

	conf, err = config.New()
	if err != nil {

		log.Panicln(err)
	}

}

func main() {

	// for i := 0; i < 2; i++ {

	phnums, err = dbhnadler.GetPhones(conf.Database, "SELECT phone FROM smsout WHERE pass=0 and block=0 and callsout=0 and provider is NULL order by cdate desc LIMIT 20")
	var phnumsplits [][]string

	for _, phnum := range phnums {

		phnumsplit, err = splitphonenum.Splite(phnum)
		if err != nil {
			log.Panicln(err)
		}
		phnumsplits = append(phnumsplits, phnumsplit)

	}

	allocCtx, allocancel := chromedp.NewRemoteAllocator(context.Background(), "ws://127.0.0.1:9222/devtools/browser/c812bfa7-e2b3-4c99-bd9a-43e33016a891")

	ctx, ctxcancel := chromedp.NewContext(allocCtx)

	// ctx, cancel := chromedp.NewContext(
	// 	context.Background(),
	// 	chromedp.WithLogf(log.Printf),
	// )
	// defer cancel()

	err = chromedp.Run(ctx, chromedp.ActionFunc(func(cxt context.Context) error {
		_, err := page.AddScriptToEvaluateOnNewDocument("Object.defineProperty(navigator, 'webdriver', { get: () => false, });").Do(cxt)
		if err != nil {
			return err
		}
		return nil
	}))
	if err != nil {
		log.Fatalln(err)
	}

	task := chromedp.Tasks{
		// network.Enable(),
		// network.SetExtraHTTPHeaders(network.Headers(headers)),

		chromedp.Navigate("http://www.siirretytnumerot.fi"),
		chromedp.ActionFunc(func(c context.Context) error {

			for i, element := range phnumsplits {

				chromedp.Sleep(4 * time.Second).Do(c)

				chromedp.SendKeys(`document.querySelector("#AutoNumber1 > tbody > tr > td > div:nth-child(1) > div:nth-child(2) > select")`, element[0], chromedp.ByJSPath).Do(c)
				chromedp.Sleep(2 * time.Second).Do(c)
				chromedp.SendKeys(`document.querySelector("#AutoNumber1 > tbody > tr > td > div:nth-child(2) > div:nth-child(2) > input")`, element[1], chromedp.ByJSPath).Do(c)
				chromedp.Sleep(2 * time.Second).Do(c)
				chromedp.Click(`//*[@id="AutoNumber1"]/tbody/tr/td/div[4]/input[1]`).Do(c)
				chromedp.Sleep(3 * time.Second).Do(c)
				chromedp.Nodes("img", &nodes).Do(c)
				chromedp.Sleep(3 * time.Second).Do(c)
				chromedp.Click(`/html/body/table/tbody/tr[3]/td[2]/ul/li[2]/a`).Do(c)
				chromedp.Sleep(3 * time.Second).Do(c)

				log.Println("NODESNUM?", len(nodes), phnums[i])
				if len(nodes) > 0 {
					elabnodes.Elab(conf, phnums[i], nodes)

				} else {

					log.Println("!!!No img nodes")
				}

			}

			return nil
		}),
	}

	if err := chromedp.Run(ctx, task); err != nil {
		log.Fatal(err)
	}

	allocancel()
	ctxcancel()

}
