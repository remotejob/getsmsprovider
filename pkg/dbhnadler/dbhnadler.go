package dbhnadler

import "database/sql"

func UpdateProvider(db *sql.DB, sqlStatement string, provider string,phone string) error {

	stmt, err := db.Prepare(sqlStatement)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(provider,phone)
	if err != nil {
		return err
	}

	return nil

}

func GetPhones(db *sql.DB, sqlStatement string) ([]string, error) {

	var res []string

	row, err := db.Query(sqlStatement)
	if err != nil {
		return res, err
	}
	defer row.Close()

	for row.Next() {

		var phone string

		row.Scan(&phone)

		res = append(res, phone)

	}

	return res, nil

}
