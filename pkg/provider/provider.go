package provider

import (
	"errors"
	"strings"
)

func Strip(text string) (string, error) {

	var provider string
	var err error = nil

	if strings.Contains(text,"Saunalahti") {

		provider = "Saunalahti"

	} else if strings.Contains(text,"Telia") {

		provider = "Telia"

	} else if strings.Contains(text,"DNA") {

		provider = "DNA"

	} else if strings.Contains(text,"Elisa") {

		provider = "Elisa"

	}  else if strings.Contains(text,"Setera") {

		provider = "Setera"

	}  else if strings.Contains(text,"EMO") {

		provider = "EMO"

	}   else if strings.Contains(text,"Twilio") {

		provider = "Twilio"

	}


	if provider == "" {
		err = errors.New(text)

	}

	return provider, err

}
