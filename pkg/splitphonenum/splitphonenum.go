package splitphonenum

import (
	"strings"
)

func Splite(phnum string) ([]string, error) {

	var res []string

	if strings.HasPrefix(phnum, "358400") {

		res = []string{"0400", strings.Replace(phnum, "358400", "", 1)}

		return res, nil

	}
	if strings.HasPrefix(phnum, "358500") {

		res = []string{"0500", strings.Replace(phnum, "358500", "", 1)}

		return res, nil

	}

	if strings.HasPrefix(phnum, "35840") {

		res = []string{"040", strings.Replace(phnum, "35840", "", 1)}

	} else if strings.HasPrefix(phnum, "35841") {

		res = []string{"041", strings.Replace(phnum, "35841", "", 1)}

	} else if strings.HasPrefix(phnum, "35842") {

		res = []string{"042", strings.Replace(phnum, "35842", "", 1)}

	} else if strings.HasPrefix(phnum, "35843") {

		res = []string{"043", strings.Replace(phnum, "35843", "", 1)}

	} else if strings.HasPrefix(phnum, "35844") {

		res = []string{"044", strings.Replace(phnum, "35844", "", 1)}

	} else if strings.HasPrefix(phnum, "35845") {

		res = []string{"045", strings.Replace(phnum, "35845", "", 1)}

	} else if strings.HasPrefix(phnum, "35846") {

		res = []string{"046", strings.Replace(phnum, "35846", "", 1)}

	} else if strings.HasPrefix(phnum, "35850") {

		res = []string{"050", strings.Replace(phnum, "35850", "", 1)}

	}

	return res, nil

}
