package elabnodes

import (
	"log"
	"os"
	"strings"

	"github.com/chromedp/cdproto/cdp"
	"gitlab.com/remotejob/getsmsprovider/internal/config"
	"gitlab.com/remotejob/getsmsprovider/internal/utils"
	"gitlab.com/remotejob/getsmsprovider/pkg/dbhnadler"
	"gitlab.com/remotejob/getsmsprovider/pkg/downloadFile"
	"gitlab.com/remotejob/getsmsprovider/pkg/ocr"
	"gitlab.com/remotejob/getsmsprovider/pkg/provider"
)

const img = "img.gif"

func Elab(conf *config.Config, phnum string, nodes []*cdp.Node) {

	if len(nodes) == 1 {

		log.Println("Nodes ==1!!! must be 2",phnum)
		err := dbhnadler.UpdateProvider(conf.Database, "update smsout set smsresult=smsresult + 1,provider=? where phone=?", "UNKNOWN", phnum)
		if err != nil {

			log.Panicln(err)
		}


	}

	for _, n := range nodes {
		link := n.AttributeValue("src")
		if strings.HasPrefix(link, "QueryServlet") {

			exist := utils.FileExists(img)

			if exist {

				e := os.Remove(img)
				if e != nil {
					log.Fatal(e)
				}

			}

			downloadlink := "http://www.siirretytnumerot.fi/" + link

			err := downloadFile.Download(downloadlink, img)
			if err != nil {

				log.Panicln(err)
			} else {

				text := ocr.Gettext(img)

				provider, err := provider.Strip(text)

				if err != nil {

					log.Println("!!!!", text)
					conf.Providerslog.WriteString(text + "\n")

				} else {

	

					err := dbhnadler.UpdateProvider(conf.Database, "update smsout set provider=? where phone=?", provider, phnum)
					if err != nil {

						log.Panicln(err)
					}
				}

			}

		}
	}

}
