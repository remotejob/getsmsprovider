package ocr

import "github.com/otiai10/gosseract/v2"

func Gettext(path string) string {

	client := gosseract.NewClient()
	defer client.Close()
	client.SetImage(path)
	text, _ := client.Text()
	
	return text
	
}