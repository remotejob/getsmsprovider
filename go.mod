module gitlab.com/remotejob/getsmsprovider

go 1.14

require (
	github.com/chromedp/cdproto v0.0.0-20201009231348-1c6a710e77de
	github.com/chromedp/chromedp v0.5.3
	github.com/chromedp/sysutil v1.0.0 // indirect
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gobwas/httphead v0.0.0-20200921212729-da3d93bc3c58 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.0.4 // indirect
	github.com/mailru/easyjson v0.7.6 // indirect
	github.com/mattn/go-sqlite3 v1.14.4
	github.com/otiai10/gosseract/v2 v2.2.4
	github.com/spf13/viper v1.7.1
	golang.org/x/sys v0.0.0-20201101102859-da207088b7d1 // indirect
)
